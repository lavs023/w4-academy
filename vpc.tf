resource "aws_vpc" "vpc_luis_valencia" {
    cidr_block = "${var.vpc_cidr}"
    enable_dns_hostnames = true
    tags {
        Name = "w4.terrafomVPC"
		Owner = "luis.valencia"
    }
}

resource "aws_internet_gateway" "igw_luis_valencia" {
    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"
	tags {
	   Name = "Internet GW"
	}
}

/*
  NAT Instance
*/
resource "aws_security_group" "nat" {
    name = "vpc_nat"
    description = "Allow traffic to pass from the private subnet to the internet"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["${var.private_subnet_cidr}"]
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["${var.private_subnet_cidr}"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"

    tags {
        Name = "NATSG"
    }
}

resource "aws_instance" "nat" {
    ami = "ami-c02b04a8" # this is a special ami preconfigured to do NAT
    availability_zone = "us-east-1a"
    instance_type = "m1.small"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.nat.id}"]
    subnet_id = "${aws_subnet.pbavz.id}"
    associate_public_ip_address = true
    source_dest_check = false

    tags {
        Name = "VPC_NAT/Bastion"
    }
}

resource "aws_eip" "nat" {
    instance = "${aws_instance.nat.id}"
    vpc = true
}

/*
  Public Subnets
*/

##### Public Subnet us-east-1a
resource "aws_subnet" "pbavz" {
    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"

    cidr_block = "${var.public_subnet_cidr}"
    availability_zone = "us-east-1a"

    tags {
        Name = "Public Subnet 1a"
    }
}

# route table pbazn
resource "aws_route_table" "pbavz" {
    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.igw_luis_valencia.id}"
    }

    tags {
        Name = "Public Subnet 1a"
    }
}

resource "aws_route_table_association" "pbavz" {
    subnet_id = "${aws_subnet.pbavz.id}"
    route_table_id = "${aws_route_table.pbavz.id}"
}

########Public Subnet us-east-1f
resource "aws_subnet" "pbavza" {
    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"

    cidr_block = "${var.public_subnet1_cidr}"
    availability_zone = "us-east-1f"

    tags {
        Name = "Public Subnet 1f"
    }
}

resource "aws_route_table_association" "pbavza" {
    subnet_id = "${aws_subnet.pbavza.id}"
    route_table_id = "${aws_route_table.pbavz.id}"
}


/*
  Private Subnet
*/

######Private Subnet us-east-1a
resource "aws_subnet" "pvavz" {
    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"

    cidr_block = "${var.private_subnet_cidr}"
    availability_zone = "us-east-1a"

    tags {
        Name = "Private Subnet 1a"
    }
}

resource "aws_route_table" "pvavz" {
    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"

    route {
        cidr_block = "0.0.0.0/0"
        instance_id = "${aws_instance.nat.id}"
    }

    tags {
        Name = "Private Subnet"
    }
}

resource "aws_route_table_association" "pvavz" {
    subnet_id = "${aws_subnet.pvavz.id}"
    route_table_id = "${aws_route_table.pvavz.id}"
}

######Private Subnet us-east-1f
resource "aws_subnet" "pvavza" {
    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"

    cidr_block = "${var.private_subnet1_cidr}"
    availability_zone = "us-east-1f"

    tags {
        Name = "Private Subnet 1f"
    }
}

resource "aws_route_table_association" "pvavza" {
    subnet_id = "${aws_subnet.pvavza.id}"
    route_table_id = "${aws_route_table.pvavz.id}"
}
