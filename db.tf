/*
  Database Servers
*/
resource "aws_security_group" "SCG_DB" {
    name = "SCG_DB"
    description = "Allow incoming database connections."

    ingress { # MySQL
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        security_groups = ["${aws_security_group.wordpress.id}"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"

    tags {
        Name = "SCG_DB"
    }
}

resource "aws_instance" "db" {
    ami = "${lookup(var.amis, var.aws_region)}"
    availability_zone = "us-east-1a"
    instance_type = "t2.micro"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.SCG_DB.id}"]
    subnet_id = "${aws_subnet.pvavz.id}"
    private_ip = "192.168.20.36"

    tags {
        Name = "w4.db"
    }
}
