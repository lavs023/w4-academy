/*
  ELB
*/
resource "aws_security_group" "wordpress_elb" {
    name = "WP_ELB"
    description = "ELB Security Group"
	vpc_id = "${aws_vpc.vpc_luis_valencia.id}"

    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }


    egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
#Aplication Load Balancer
 
resource "aws_alb" "ELB_WP" {
  name = "AlbWP"
  internal = false
  # Public subnets
  subnets = ["${aws_subnet.pbavz.id}", "${aws_subnet.pbavza.id}"]
  security_groups = ["${aws_security_group.wordpress_elb.id}"]

}

resource "aws_alb_target_group" "WebWP" {
    name = "WPServers"
    port = 80
    protocol = "HTTP"
    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"
	
	health_check {
    healthy_threshold   = 2
    interval            = 15
    path                = "/"
    timeout             = 10
    unhealthy_threshold = 2
  }
}

#Listeners

resource "aws_alb_listener" "wp-https" {
   load_balancer_arn = "${aws_alb.ELB_WP.arn}"
   port = "443"
   protocol = "HTTPS"
   ssl_policy = "ELBSecurityPolicy-2016-08"
   certificate_arn = "arn:aws:acm:us-east-1:509130302659:certificate/3b7ce0d0-1584-4398-b180-3dacf4236937"
   
   default_action {
     target_group_arn = "${aws_alb_target_group.WebWP.arn}"
     type = "forward"
   }
}

#Instance Attachment
resource "aws_alb_target_group_attachment" "WP_instances" {
  target_group_arn = "${aws_alb_target_group.WebWP.arn}"
  target_id        = "${aws_instance.w4_wordpress.id}"  
  port             = 80
}
 



/*
  DNS to ELB
*/

resource "aws_route53_record" "lv" {
  zone_id = "Z2Q6MDUSLN1HKB"
  name    = "lv.glbacademies.com"
  type    = "A"
  
  alias {
    name                   = "${aws_alb.ELB_WP.dns_name}"
	zone_id                = "${aws_alb.ELB_WP.zone_id}"
	evaluate_target_health = true
  }
}

