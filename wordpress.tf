/*
  Wordpress Servers
*/
resource "aws_security_group" "wordpress" {
    name = "SCG_wordpress"
    description = "Allow incoming HTTP connections."

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
	ingress {
        from_port   = "22"
        to_port     = "22"
        protocol    = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
       from_port = 0
       to_port = 0
       protocol = "-1"
       cidr_blocks = ["0.0.0.0/0"]
    }
    vpc_id = "${aws_vpc.vpc_luis_valencia.id}"

    tags {
        Name = "SCG_wordpress"
    }
}

resource "aws_instance" "w4_wordpress" {
    ami = "${lookup(var.amis, var.aws_region)}"
    availability_zone = "us-east-1a"
    instance_type = "t2.micro"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.wordpress.id}"]
    subnet_id = "${aws_subnet.pvavz.id}"
    private_ip = "192.168.20.37"


    tags {
        Name  = "w4.wordpress"
		Owner = "luis.valencia"
    }
}

